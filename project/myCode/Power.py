class Power:
	def __init__(self, base, exponent):
		self.b = base
		self.__e = exponent

	def compute(self):
		return self.b ** self.__e

	def getExponent(self):
		return self.__e
